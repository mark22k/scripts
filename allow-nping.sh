ALIAS_CMD="alias nping='/usr/bin/nping --privileged'"
RC_FILE=~/.bashrc

sudo setcap cap_net_raw+ep $(realpath $(which nping))
getcap $(realpath $(which nping))

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

