ALIAS_CMD="alias scp='$(which scp) -C'"
RC_FILE=~/.bashrc

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

