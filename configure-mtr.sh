ALIAS_CMD="alias mtr='$(which mtr) -t'"
RC_FILE=~/.bashrc

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

