ALIAS_CMD="alias tracepath='$(which tracepath) -p 33434'"
RC_FILE=~/.bashrc

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

