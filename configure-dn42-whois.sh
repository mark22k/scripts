cat > /etc/whois.conf <<EOF
# dn42 domains
\.dn42$           whois.bandura.dn42
\.neo$            whois.bandura.dn42
\.crxn$           whois.bandura.dn42
\.hack$           whois.bandura.dn42$

# Entities
\-DN42$           whois.bandura.dn42
\-NEONETWORK$     whois.bandura.dn42
\-CRXN$           whois.bandura.dn42
\-CHAOSVPN$       whois.bandura.dn42

# dn42 range 64512-65534
^as6(4(5(1[2-9]|[2-9][0-9])|[6-9][0-9]{2})|5([0-4][0-9]{2}|5([0-2][0-9]|3[0-4])))$ whois.bandura.dn42
# dn42 range 76100-76199
^as761[0-9][0-9]$   whois.bandura.dn42
# dn42 range 4242420000-4242429999
^as424242[0-9]{4}$ whois.bandura.dn42
# NeoNetwork range
^as420127[0-9]{4}$ whois.bandura.dn42

# dn42 ipv4 address space
^172\.2[0-3]\.[0-9]{1,3}\.[0-9]{1,3}(\/(1[56789]|2[0-9]|3[012]))?$ whois.bandura.dn42
# NeoNetwork ipv4 address space
^10\.127\.[0-9]{1,3}\.[0-9]{1,3}(\/(1[56789]|2[0-9]|3[012]))?$ whois.bandura.dn42


# dn42 ula ipv6 address space
^fd**:****:****:****:****:****:****:**** whois.bandura.dn42
EOF
