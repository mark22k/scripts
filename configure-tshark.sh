ALIAS_CMD="alias tshark='$(which tshark) --color'"
RC_FILE=~/.bashrc

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

