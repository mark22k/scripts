ALIAS_CMD="alias nmap='/usr/bin/nmap --privileged'"
RC_FILE=~/.bashrc

sudo setcap cap_net_raw+ep $(realpath $(which nmap))
getcap $(realpath $(which nmap))

if ! grep -q "$ALIAS_CMD" "$RC_FILE"; then
    echo $ALIAS_CMD >> $RC_FILE
fi

