
echo "Mirror mrt.collector.dn42."
echo "This might take a while."
# Try to mirror the newest files
if wget --mirror --recursive --page-requisites --level=3 -D mrt.collector.dn42 --tries=5 --continue --timeout=10 --waitretry=60 --prefer-family=IPv4 --retry-connrefused --local-encoding=UTF-8 --compression=auto --secure-protocol=TLSv1_3 https://mrt.collector.dn42/; then
    echo "Mirror successful."
else
    echo "Failed to mirror. Please investigate."
fi

# Remove html files so that wget
# follow all links when mirroring again
# see https://stackoverflow.com/questions/13092229/cant-resume-wget-mirror-with-no-clobber-c-f-b-unhelpful
echo "Remove files."
find mrt.collector.dn42 -name "*.html" -type f -delete 

