if [ ! "$1" ]; then
  echo "Adds a user to the WireShark group so that he is allowed to capture packets."
fi

sudo adduser $1 wireshark
